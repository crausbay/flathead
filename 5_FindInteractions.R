library(tidyverse) 
library(dismo)
library(ggplot2)

#### Find Ecodrought interactions ####

#read the rds
FH_06 <- readRDS("FH_06.rds")
FH_07 <- readRDS("FH_07.rds")
FH_08 <- readRDS("FH_08.rds")
FH_09 <- readRDS("FH_09.rds")
FH_10 <- readRDS("FH_10.rds")
FH_11 <- readRDS("FH_11.rds")
FH_12 <- readRDS("FH_12.rds")

####FH_11####
#characterizing interactions
FH_11.interactions <- gbm.interactions(FH_11)
FH_11.interactions$interactions
FH_11.interactions$rank.list
list.FH_11 <- FH_11.interactions$interactions
write.csv(list.FH_11, "FH_11_InteractionsList.csv")

####repeat for the remaining hucs, or turn the above into a loop!