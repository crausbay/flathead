Scripts for ecodrought vulnerability analysis in the Flathead

1_Ecodrought_SummarizeData.R - this script 1) summarizes the mean, min, max for each covariate, 2) removes covariates that you select (in our case, years that aren't characterized by drought), 3) calculates collinearity, and 4) reports the number of pixels in each huc8

2_Ecodrought_FindBestClim.R - this script runs BRT models that will help us select among years within a single covariate category, e.g., EDDI.  It runs models with all the ecological variables, and landscape variables, and then adds only the EDDI years, for example. THen a second model will run all ecological and landscape variables plus only the LERI years, and so forth.  We select the year for each climate covariate with the highest relative importance in these models, to put forward in the next script.

3_Ecodrought_BRT.R - this script runs BRT models for a single huc8 with ecological, landscape variables, + the best of swe, spi, leri, and eddi. It saves the importance points for each covariate, and plots a smooth fitted response for each covariate

4_Calculate_VIF.R - this script calculates the variable inflation score. Scores >5 are suspect.

5_FindInteractions.R - this script finds interactions between two covariates and writes a list of these interactions

6_Plot_BRT_Fitted_Functions.R - this script plots fitted functions across all huc8 models when given a .csv file that organizes the fitted fundtion values for a particular covariate, e.g., canopy cover, for all models. this .csv file must also report the relative importance of that covariate for that huc8 model as a Size for plotting the line thickness, such that thicker lines show situations where the covariate was more important in that model.

7_Plot_BRT_Interactions.R - this script plots the interactions that are specified.

In addition, the function 'modified_gbm.perspec' is included for graphing.
